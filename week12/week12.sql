select * from movies;

select * from denormalized;

show variables like "secure_file_priv";

load data 
infile "C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\denormalized_movie_db.csv"
into table denormalized
columns terminated by ';';

select movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

insert into movies (movie_id, title, ranking, rating, year, votes, duration, oscars, budget)
select distinct movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

select * from movies;

delete from movies; 




